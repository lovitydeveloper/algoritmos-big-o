const Solucion = require('./practica');

describe('Solucion', () => {
    describe('calcularAcumulacionNieve', () => {
        test('debería calcular correctamente la acumulación de nieve', () => {
            const solucion = new Solucion();
            const paredes = [4, 2, 3, 4, 1, 2, 1, 3, 2];
            const resultadoEsperado = 14;
            const resultadoActual = solucion.calcularAcumulacionNieve(paredes);
            console.log('resultadoActual',resultadoActual);
            expect(resultadoActual).toEqual(resultadoEsperado);
        });
    });
});
