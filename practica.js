class Solucion {

    /**
     * Problema 1:
     * A partir de un array de enteros, cuyos valores representan la altura de paredes, 
     * encontrar la cantidad de nieve que se acumula entre ellas de la forma mas eficiente 
     * y especificar cual clasificación Big O posee la solución.
     * Fuente: youtube.com/watch?v=ScJPMyn94G4
     */
    calcularAcumulacionNieve(paredes) {
        let totalNieveAcumulada = 0;
        let left = 0;
        let right = paredes.length - 1;
        let maxLeft = 0;
        let maxRight = 0;

        while (left < right) {
            if (paredes[left] <= paredes[right]) {
                if (paredes[left] >= maxLeft) {
                    maxLeft = paredes[left];
                } else {
                    totalNieveAcumulada += maxLeft - paredes[left];
                }
                left++;
            } else {
                if (paredes[right] >= maxRight) {
                    maxRight = paredes[right];
                } else {
                    totalNieveAcumulada += maxRight - paredes[right];
                }
                right--;
            }
        }

        return totalNieveAcumulada;
    }
    /**
     * Explicación y desarrollo: 
     * Este algoritmo utiliza dos punteros, uno para recorrer el array desde la izquierda (left) 
     * y otro para recorrerlo desde la derecha (right). En cada iteración, se compara la altura 
     * de las paredes representadas por los punteros. Se mantiene un seguimiento de la altura 
     * máxima encontrada hasta el momento tanto para la pared izquierda (maxLeft) como para 
     * la pared derecha (maxRight).
     * La complejidad de este algoritmo es de O(n), donde "n" es el número de elementos en el array 
     * de paredes. Esto se debe a que recorremos el array una sola vez con los punteros left y right, 
     * realizando una cantidad constante de operaciones en cada iteración. Por lo tanto, el algoritmo 
     * es eficiente y puede manejar grandes conjuntos de datos en un tiempo razonable.
     */

    /**
     * Problema 2: 
     * Otro enunciado
     */
    agregarOtraPrueba() {
        // el algoritmo solucion aquí
    }
    /**
     * Explicación y desarrollo: 
     * 
     */
}
module.exports = Solucion;

