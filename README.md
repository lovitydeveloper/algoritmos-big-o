# Algoritmos `Big O`

En este repositorio encontrarás algunos ejemplos prácticos de problemas clásicos y sus resoluciones donde se especifica el tipo de algoritmo Big O utilizado.

## Uso

1. Luego de clonar este repo y posicionarte en el directorio de trabajo, desde linea de comandos instala las dependencias ejecutando:
`npm install` 

2. Practicar dentro del archivo `practica.js` escribiendo el enunciado y a continuación, el método cuyo codigo da solución. Finalmente puedes agregar una descripción de su funcionamiento a modo de conflusión, por debajo de dicho método en un comentario de párrafo.

3. Escribe el **uni test** dentro del archivo `practica.test.js`.

4. Prueba la solución usando jest ejecutando:
`npm test` (saldrá el resultado en la consola)

5. Puedes revisar la covertura de pruebas ejecutando:
`npm run test:coverage` (saldrá el resultado en la consola)
Tambien puedes obtener un reporte web, luego de ejecutar este script visitando en tu navegador el directorio local que genera este proceso en `coverage/lcov-report/index.html`

## Notación `Big O`

La notación Big O proporciona una forma de describir la eficiencia relativa de los algoritmos en términos de tiempo de ejecución en función del tamaño de las estructuras de datos.

- O(1) - Tiempo constante: el tiempo de ejecución no depende del tamaño de entrada. Ejemplos de esto pueden ser operaciones aritméticas simples, acceso a un elemento de una matriz por índice conocido, entre otros.

- O(log n) - Tiempo logarítmico: el tiempo de ejecución disminuye a medida que aumenta el tamaño de entrada, pero la disminución es cada vez menor. Ejemplos de esto son algoritmos de búsqueda binaria.

- O(n) - Tiempo lineal: el tiempo de ejecución aumenta proporcionalmente al tamaño de entrada. Ejemplos de esto son bucles for que recorren una matriz una vez.

- O(n^2) - Tiempo cuadrático: el tiempo de ejecución aumenta al cuadrado en relación con el tamaño de entrada. Ejemplos de esto pueden ser bucles anidados que recorren una matriz.

- O(2^n) - Tiempo exponencial: el tiempo de ejecución aumenta exponencialmente con el tamaño de entrada. Ejemplos de esto son algoritmos recursivos ineficientes.

Esta es la clasificación basica, luego puede haber convinaciones de ellos.

## Tipos más comunes de algoritmos, clasificacion `Big O` y ejemplos

1. **Búsqueda lineal (Linear Search):** Un algoritmo simple que busca un elemento en una lista de manera secuencial, recorriendo cada elemento uno por uno hasta encontrar una coincidencia.

`O(n)`
El tiempo de ejecución de la búsqueda lineal es proporcional al tamaño de la lista n, ya que en el peor caso puede ser necesario recorrer todos los elementos de la lista.

```javascript
function linearSearch(arr, target) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === target) {
      return i; // devuelve el índice si se encuentra el elemento
    }
  }
  return -1; // devuelve -1 si el elemento no se encuentra
}

const array = [5, 3, 8, 2, 4];
console.log(linearSearch(array, 8)); // Output: 2
```

2. **Búsqueda binaria (Binary Search):** Un algoritmo eficiente para buscar elementos en una lista ordenada. Divide repetidamente la lista por la mitad y determina en qué mitad se encuentra el elemento buscado.

`O(log n)`
La búsqueda binaria reduce el espacio de búsqueda a la mitad en cada iteración, lo que resulta en un tiempo de ejecución logarítmico en función del tamaño de la lista.

```javascript
function binarySearch(arr, target) {
  let low = 0;
  let high = arr.length - 1;

  while (low <= high) {
    let mid = Math.floor((low + high) / 2);
    if (arr[mid] === target) {
      return mid; // devuelve el índice si se encuentra el elemento
    } else if (arr[mid] < target) {
      low = mid + 1;
    } else {
      high = mid - 1;
    }
  }

  return -1; // devuelve -1 si el elemento no se encuentra
}

const array = [2, 4, 6, 8, 10];
console.log(binarySearch(array, 6)); // Output: 2
```

3. **Ordenamiento de burbuja (Bubble Sort):** Un algoritmo de ordenamiento elemental que compara y intercambia repetidamente elementos adyacentes hasta que toda la lista esté ordenada.

`O(n^2)`
En el peor caso, el ordenamiento de burbuja requiere comparar y mover elementos en todas las posibles combinaciones, lo que da lugar a una complejidad cuadrática.

```javascript
function bubbleSort(arr) {
  const length = arr.length;
  for (let i = 0; i < length - 1; i++) {
    for (let j = 0; j < length - 1 - i; j++) {
      if (arr[j] > arr[j + 1]) {
        // Intercambia elementos si están en el orden incorrecto
        [arr[j], arr[j + 1]] = [arr[j + 1], arr[j]];
      }
    }
  }
  return arr;
}

const array = [5, 3, 8, 2, 4];
console.log(bubbleSort(array)); // Output: [2, 3, 4, 5, 8]
```

4. **Ordenamiento por inserción (Insertion Sort):** Un algoritmo de ordenamiento que construye una lista ordenada de manera incremental, insertando elementos en su posición correcta dentro de la lista.

`O(n^2)`
El ordenamiento por inserción también implica comparar y mover elementos en combinaciones, aunque es más eficiente que el ordenamiento de burbuja en algunos casos debido a su estrategia de inserción.

```javascript
function insertionSort(arr) {
  const length = arr.length;
  for (let i = 1; i < length; i++) {
    let current = arr[i];
    let j = i - 1;
    while (j >= 0 && arr[j] > current) {
      arr[j + 1] = arr[j];
      j--;
    }
    arr[j + 1] = current;
  }
  return arr;
}

const array = [5, 3, 8, 2, 4];
console.log(insertionSort(array)); // Output: [2, 3, 4, 5, 8]
```

5. **Ordenamiento por mezcla (Merge Sort):** Un algoritmo de ordenamiento recursivo que divide la lista en partes más pequeñas, las ordena por separado y luego las fusiona para obtener una lista ordenada completa.

`O(n log n)`
El ordenamiento por mezcla divide la lista en mitades hasta llegar a sublistas de un solo elemento y luego fusiona esas sublistas en una lista ordenada. Su complejidad se basa en la combinación logarítmica y el tiempo lineal de fusión.

```javascript
function mergeSort(arr) {
  if (arr.length <= 1) {
    return arr;
  }

  const mid = Math.floor(arr.length / 2);
  const left = arr.slice(0, mid);
  const right = arr.slice(mid);

  return merge(mergeSort(left), mergeSort(right));
}

function merge(left, right) {
  let result = [];
  let i = 0;
  let j = 0;

  while (i < left.length && j < right.length) {
    if (left[i] < right[j]) {
      result.push(left[i]);
      i++;
    } else {
      result.push(right[j]);
      j++;
    }
  }

  return result.concat(left.slice(i)).concat(right.slice(j));
}

const array = [64, 34, 25, 12, 22, 11, 90];
console.log(mergeSort(array)); // Output: [11, 12, 22, 25, 34, 64, 90]
```

6. **Algoritmo de búsqueda en profundidad (Depth-First Search, DFS):** Un algoritmo utilizado para recorrer o buscar elementos en una estructura de datos, como un grafo, de manera profunda, visitando todos los nodos adyacentes antes de retroceder.

`O(V + E)`
En un grafo con V vértices y E aristas, el tiempo de ejecución del DFS es proporcional al número total de vértices y aristas visitados.

```javascript
class Graph {
  constructor() {
    this.vertices = [];
    this.adjList = new Map();
  }

  addVertex(vertex) {
    this.vertices.push(vertex);
    this.adjList.set(vertex, []);
  }

  addEdge(vertex1, vertex2) {
    this.adjList.get(vertex1).push(vertex2);
    this.adjList.get(vertex2).push(vertex1);
  }

  dfs(startingVertex) {
    const visited = {};
    for (let i = 0; i < this.vertices.length; i++) {
      visited[this.vertices[i]] = false;
    }

    this.dfsUtil(startingVertex, visited);
  }

  dfsUtil(vertex, visited) {
    visited[vertex] = true;
    console.log(vertex);

    const neighbors = this.adjList.get(vertex);
    for (let i = 0; i < neighbors.length; i++) {
      const currentNeighbor = neighbors[i];
      if (!visited[currentNeighbor]) {
        this.dfsUtil(currentNeighbor, visited);
      }
    }
  }
}

// Crear un grafo
const graph = new Graph();

// Agregar vértices
graph.addVertex('A');
graph.addVertex('B');
graph.addVertex('C');
graph.addVertex('D');
graph.addVertex('E');

// Agregar aristas
graph.addEdge('A', 'B');
graph.addEdge('A', 'C');
graph.addEdge('B', 'D');
graph.addEdge('D', 'E');

// Realizar la búsqueda en profundidad
graph.dfs('A');
```

7. **Algoritmo de búsqueda en anchura (Breadth-First Search, BFS):** Un algoritmo utilizado para recorrer o buscar elementos en una estructura de datos, como un grafo, de manera amplia, visitando todos los nodos del nivel actual antes de pasar al siguiente nivel.

`O(V + E)`
Al igual que DFS, el BFS también tiene una complejidad de tiempo proporcional al número total de vértices y aristas visitados en un grafo.

```javascript
// Definición de la clase Nodo
class Nodo {
  constructor(valor) {
    this.valor = valor;
    this.vecinos = [];
    this.visitado = false;
  }

  agregarVecino(vecino) {
    this.vecinos.push(vecino);
  }
}

// Implementación del algoritmo BFS
function bfs(inicio) {
  const cola = [];
  cola.push(inicio);
  inicio.visitado = true;

  while (cola.length > 0) {
    const nodoActual = cola.shift();
    console.log(nodoActual.valor);

    nodoActual.vecinos.forEach(vecino => {
      if (!vecino.visitado) {
        cola.push(vecino);
        vecino.visitado = true;
      }
    });
  }
}

// Creación del grafo
const nodoA = new Nodo('A');
const nodoB = new Nodo('B');
const nodoC = new Nodo('C');
const nodoD = new Nodo('D');
const nodoE = new Nodo('E');
const nodoF = new Nodo('F');

nodoA.agregarVecino(nodoB);
nodoA.agregarVecino(nodoC);
nodoB.agregarVecino(nodoD);
nodoB.agregarVecino(nodoE);
nodoC.agregarVecino(nodoE);
nodoC.agregarVecino(nodoF);
nodoD.agregarVecino(nodoF);

// Llamada al algoritmo BFS desde el nodo A
console.log("Recorrido BFS:");
bfs(nodoA);
```

8. **Algoritmo de árbol de búsqueda binaria (Binary Search Tree, BST):** Una estructura de datos en forma de árbol que permite una búsqueda eficiente, inserción y eliminación de elementos. Los nodos en el árbol se organizan de manera que los nodos en el subárbol izquierdo sean menores que el nodo raíz, y los nodos en el subárbol derecho sean mayores.

`O(log n)` (en promedio)
El tiempo de búsqueda en un BST está determinado por la altura del árbol, que en promedio es logarítmica en función del número de nodos.

```javascript
// Definición de la clase Nodo
class Nodo {
  constructor(valor) {
    this.valor = valor;
    this.izquierdo = null;
    this.derecho = null;
  }
}

// Definición de la clase Árbol de búsqueda binaria
class ArbolBinarioBusqueda {
  constructor() {
    this.raiz = null;
  }

  // Método para insertar un valor en el árbol
  insertar(valor) {
    const nuevoNodo = new Nodo(valor);

    if (this.raiz === null) {
      this.raiz = nuevoNodo;
    } else {
      this.insertarNodo(this.raiz, nuevoNodo);
    }
  }

  // Método auxiliar para insertar un nodo en el árbol
  insertarNodo(nodo, nuevoNodo) {
    if (nuevoNodo.valor < nodo.valor) {
      if (nodo.izquierdo === null) {
        nodo.izquierdo = nuevoNodo;
      } else {
        this.insertarNodo(nodo.izquierdo, nuevoNodo);
      }
    } else {
      if (nodo.derecho === null) {
        nodo.derecho = nuevoNodo;
      } else {
        this.insertarNodo(nodo.derecho, nuevoNodo);
      }
    }
  }

  // Método para buscar un valor en el árbol
  buscar(valor) {
    return this.buscarNodo(this.raiz, valor);
  }

  // Método auxiliar para buscar un nodo en el árbol
  buscarNodo(nodo, valor) {
    if (nodo === null) {
      return false;
    }

    if (valor === nodo.valor) {
      return true;
    } else if (valor < nodo.valor) {
      return this.buscarNodo(nodo.izquierdo, valor);
    } else {
      return this.buscarNodo(nodo.derecho, valor);
    }
  }

  // Método para recorrer el árbol en orden (in-order traversal)
  recorrerInOrder() {
    this.recorrerInOrderNodo(this.raiz);
  }

  // Método auxiliar para recorrer el árbol en orden
  recorrerInOrderNodo(nodo) {
    if (nodo !== null) {
      this.recorrerInOrderNodo(nodo.izquierdo);
      console.log(nodo.valor);
      this.recorrerInOrderNodo(nodo.derecho);
    }
  }
}

// Ejemplo de uso del árbol de búsqueda binaria
const arbol = new ArbolBinarioBusqueda();

arbol.insertar(8);
arbol.insertar(3);
arbol.insertar(10);
arbol.insertar(1);
arbol.insertar(6);
arbol.insertar(14);
arbol.insertar(4);
arbol.insertar(7);
arbol.insertar(13);

console.log("Recorrido in-order:");
arbol.recorrerInOrder();

console.log("Buscar 6:", arbol.buscar(6));
console.log("Buscar 12:", arbol.buscar(12));
```

9. **Algoritmo de Kruskal:** Un algoritmo utilizado para encontrar un árbol de expansión mínima en un grafo ponderado. Es útil en problemas relacionados con redes y conexiones de costo mínimo.

`O(E log E)` o `O(E log V)`
El tiempo de ejecución del algoritmo de Kruskal para encontrar el árbol de expansión mínima en un grafo ponderado está determinado por el número de aristas E y el número de vértices V.

```javascript
// Definición de la clase Grafo
class Grafo {
  constructor(numVertices) {
    this.numVertices = numVertices;
    this.aristas = [];
  }

  // Método para agregar una arista al grafo
  agregarArista(origen, destino, peso) {
    this.aristas.push([origen, destino, peso]);
  }

  // Método para encontrar el subconjunto al que pertenece un vértice
  encontrar(subconjuntos, vertice) {
    if (subconjuntos[vertice] === undefined) {
      return vertice;
    }

    return this.encontrar(subconjuntos, subconjuntos[vertice]);
  }

  // Método para unir dos subconjuntos en un solo conjunto
  unir(subconjuntos, vertice1, vertice2) {
    const raizVertice1 = this.encontrar(subconjuntos, vertice1);
    const raizVertice2 = this.encontrar(subconjuntos, vertice2);
    subconjuntos[raizVertice1] = raizVertice2;
  }

  // Método para ejecutar el algoritmo de Kruskal
  kruskal() {
    const arbolExpansionMinima = [];

    // Ordenar las aristas en orden creciente de peso
    this.aristas.sort((a, b) => a[2] - b[2]);

    // Inicializar los subconjuntos
    const subconjuntos = {};

    // Iterar sobre las aristas ordenadas
    for (let i = 0; i < this.aristas.length; i++) {
      const [origen, destino, peso] = this.aristas[i];

      // Comprobar si agregar la arista forma un ciclo
      if (this.encontrar(subconjuntos, origen) !== this.encontrar(subconjuntos, destino)) {
        // Agregar la arista al árbol de expansión mínima
        arbolExpansionMinima.push([origen, destino, peso]);

        // Unir los subconjuntos
        this.unir(subconjuntos, origen, destino);
      }
    }

    return arbolExpansionMinima;
  }
}

// Ejemplo de uso del algoritmo de Kruskal
const grafo = new Grafo(6);

grafo.agregarArista(0, 1, 4);
grafo.agregarArista(0, 2, 3);
grafo.agregarArista(1, 2, 1);
grafo.agregarArista(1, 3, 2);
grafo.agregarArista(2, 3, 4);
grafo.agregarArista(3, 4, 2);
grafo.agregarArista(4, 5, 6);

const arbolExpansionMinima = grafo.kruskal();

console.log("Árbol de expansión mínima:");
arbolExpansionMinima.forEach(([origen, destino, peso]) => {
  console.log(`${origen} -- ${destino} (peso: ${peso})`);
});
```

10. **Algoritmo de Dijkstra:** Un algoritmo utilizado para encontrar el camino más corto entre dos nodos en un grafo ponderado. Es comúnmente utilizado en problemas de optimización de rutas y planificación de rutas.

`O((V + E) log V)` o `O((V + E) log E)`
El algoritmo de Dijkstra tiene una complejidad de tiempo dependiente del número de vértices y aristas en el grafo. El uso de una cola de prioridad basada en un montículo binario puede llevar a complejidades ligeramente diferentes según la implementación.

```javascript
// Definición de la clase Grafo
class Grafo {
  constructor() {
    this.nodos = new Map();
  }

  // Método para agregar un nodo al grafo
  agregarNodo(nodo) {
    this.nodos.set(nodo, new Map());
  }

  // Método para agregar una arista al grafo
  agregarArista(origen, destino, peso) {
    this.nodos.get(origen).set(destino, peso);
    this.nodos.get(destino).set(origen, peso);
  }

  // Método para encontrar el nodo con la distancia mínima
  encontrarNodoDistanciaMinima(distancias, visitados) {
    let nodoMinimo = null;
    let distanciaMinima = Infinity;

    for (const [nodo, distancia] of distancias) {
      if (!visitados.has(nodo) && distancia < distanciaMinima) {
        nodoMinimo = nodo;
        distanciaMinima = distancia;
      }
    }

    return nodoMinimo;
  }

  // Método para encontrar la ruta más corta utilizando el algoritmo de Dijkstra
  encontrarRutaMasCorta(origen) {
    const distancias = new Map();
    const visitados = new Set();
    const rutaAnterior = new Map();

    // Inicializar las distancias con infinito para todos los nodos excepto el origen
    for (const nodo of this.nodos.keys()) {
      if (nodo === origen) {
        distancias.set(nodo, 0);
      } else {
        distancias.set(nodo, Infinity);
      }
    }

    while (visitados.size < this.nodos.size) {
      const nodoActual = this.encontrarNodoDistanciaMinima(distancias, visitados);
      visitados.add(nodoActual);

      for (const [nodoVecino, peso] of this.nodos.get(nodoActual)) {
        const distanciaAcumulada = distancias.get(nodoActual) + peso;

        if (distanciaAcumulada < distancias.get(nodoVecino)) {
          distancias.set(nodoVecino, distanciaAcumulada);
          rutaAnterior.set(nodoVecino, nodoActual);
        }
      }
    }

    return { distancias, rutaAnterior };
  }

  // Método para reconstruir la ruta desde el origen hasta un nodo destino
  reconstruirRuta(rutaAnterior, destino) {
    const ruta = [destino];
    let nodo = destino;

    while (rutaAnterior.has(nodo)) {
      nodo = rutaAnterior.get(nodo);
      ruta.unshift(nodo);
    }

    return ruta;
  }
}

// Ejemplo de uso del algoritmo de Dijkstra
const grafo = new Grafo();

grafo.agregarNodo('A');
grafo.agregarNodo('B');
grafo.agregarNodo('C');
grafo.agregarNodo('D');
grafo.agregarNodo('E');
grafo.agregarNodo('F');

grafo.agregarArista('A', 'B', 4);
grafo.agregarArista('A', 'C', 2);
grafo.agregarArista('B', 'E', 3);
grafo.agregarArista('C', 'D', 2);
grafo.agregarArista('C', 'F', 4);
grafo.agregarArista('D', 'E', 3);
grafo.agregarArista('D', 'F', 1);
grafo.agregarArista('E', 'F', 1);

const origen = 'A';
const { distancias, rutaAnterior } = grafo.encontrarRutaMasCorta(origen);

console.log("Distancias desde el origen", origen);
for (const [nodo, distancia] of distancias) {
  console.log(\`${nodo}: ${distancia}\`);
}

const destino = 'F';
const rutaMasCorta = grafo.reconstruirRuta(rutaAnterior, destino);
console.log("Ruta más corta:", rutaMasCorta.join(' -> '));
```


# Estructuras de datos

## Tipos comunes
Algunas de las estructuras de datos más comunes son:

1. **Array (arreglo)**: Una estructura de datos que almacena elementos en una secuencia contigua de memoria.

2. **Lista enlazada**: Una estructura de datos en la que los elementos están conectados mediante nodos que contienen referencias al siguiente elemento de la lista.

3. **Pila (stack)**: Una estructura de datos que sigue la regla LIFO (Last In, First Out), donde el último elemento agregado es el primero en ser eliminado.

4. **Cola (queue)**: Una estructura de datos que sigue la regla FIFO (First In, First Out), donde el primer elemento agregado es el primero en ser eliminado.

5. **Árbol**: Una estructura de datos no lineal que consiste en un conjunto de nodos conectados mediante enlaces jerárquicos. Los árboles pueden tener diferentes formas y propiedades, como los árboles binarios y los árboles de búsqueda binaria.

6. **Grafo**: Una estructura de datos no lineal que consta de un conjunto de nodos (vértices) y conexiones (aristas) entre ellos.

7. **Hash table (tabla hash)**: Una estructura de datos que utiliza una función de hash para almacenar y recuperar datos de manera eficiente, asociando claves con valores.

8. **Heap**: Una estructura de datos especial que satisface la propiedad de heap, que puede ser un min heap (el nodo raíz es el valor mínimo) o un max heap (el nodo raíz es el valor máximo).


## Ejemplos

Ejemplos de implementaciones de cada una de las estructuras de datos mencionadas en JavaScript:

1. **Array (arreglo)**:
```javascript
const array = [1, 2, 3, 4, 5];
console.log(array); // [1, 2, 3, 4, 5]
```

2. **Lista enlazada**:
```javascript
class Node {
  constructor(data) {
    this.data = data;
    this.next = null;
  }
}

class LinkedList {
  constructor() {
    this.head = null;
  }

  append(data) {
    const newNode = new Node(data);

    if (!this.head) {
      this.head = newNode;
    } else {
      let current = this.head;
      while (current.next) {
        current = current.next;
      }
      current.next = newNode;
    }
  }
}

const linkedList = new LinkedList();
linkedList.append(1);
linkedList.append(2);
linkedList.append(3);
```

3. **Pila (stack)**:
```javascript
const stack = [];
stack.push(1);
stack.push(2);
stack.push(3);
console.log(stack.pop()); // 3
```

4. **Cola (queue)**:
```javascript
const queue = [];
queue.push(1);
queue.push(2);
queue.push(3);
console.log(queue.shift()); // 1
```

5. **Árbol**:
```javascript
class Node {
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

const rootNode = new Node(1);
rootNode.left = new Node(2);
rootNode.right = new Node(3);
```

6. **Grafo**:
```javascript
class Graph {
  constructor() {
    this.vertices = [];
    this.edges = {};
  }

  addVertex(vertex) {
    this.vertices.push(vertex);
    this.edges[vertex] = [];
  }

  addEdge(vertex1, vertex2) {
    this.edges[vertex1].push(vertex2);
    this.edges[vertex2].push(vertex1);
  }
}

const graph = new Graph();
graph.addVertex("A");
graph.addVertex("B");
graph.addEdge("A", "B");
```

7. **Hash table (tabla hash)**:
```javascript
class HashTable {
  constructor() {
    this.table = {};
  }

  hash(key) {
    let hash = 0;
    for (let i = 0; i < key.length; i++) {
      hash += key.charCodeAt(i);
    }
    return hash;
  }

  set(key, value) {
    const index = this.hash(key);
    this.table[index] = value;
  }

  get(key) {
    const index = this.hash(key);
    return this.table[index];
  }
}

const hashtable = new HashTable();
hashtable.set("name", "John");
console.log(hashtable.get("name")); // John
```

8. **Heap**:
```javascript
class MinHeap {
  constructor() {
    this.heap = [];
  }

  insert(value) {
    this.heap.push(value);
    this.bubbleUp(this.heap.length - 1);
  }

  bubbleUp(index) {
    while (index > 0) {
      const parentIndex = Math.floor((index - 1) / 2);
      if (this.heap[index] >= this.heap[parentIndex]) {
        break;
      }
      [this.heap[index], this.heap[parentIndex]] = [this.heap[parentIndex], this.heap[index]];
      index = parentIndex;
    }
  }
}

const

 minHeap = new MinHeap();
minHeap.insert(5);
minHeap.insert(3);
minHeap.insert(8);
```

Estos son ejemplos básicos de implementaciones en JavaScript de cada una de las estructuras de datos mencionadas. Ten en cuenta que estos ejemplos son simplificados y hay muchas más operaciones y funcionalidades que se pueden agregar a cada estructura de datos según los requisitos específicos.